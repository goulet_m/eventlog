﻿using System;

namespace EventLog.Core {

    public static class Extensions {

        public const string TIME_FORMAT = "yyyy-MM-ddTHH:mm:ss.000Z";

        public static string ToUniversalTimeString(this DateTime time) {
            return time.ToUniversalTime().ToString(TIME_FORMAT);
        }

    }

}
