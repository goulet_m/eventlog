﻿using System;
using System.Collections.Generic;
using SD = System.Diagnostics;

namespace EventLog.Core {

    public class EventQuery {

        #region Constants
        
        public const string QUERY_TIME_CREATED = "TimeCreated[@SystemTime>='{0}' and @SystemTime<='{1}']";

        #endregion

        #region Privates Methods

        /// <summary>
        /// Get a query to selection a timespan
        /// </summary>
        /// <param name="startTime">Starting time</param>
        /// <param name="endTime">Ending time</param>
        private static string GetTimeQuery(DateTime startTime, DateTime endTime) {

            return String.Format(QUERY_TIME_CREATED, 
                                 startTime.ToUniversalTimeString(), 
                                 endTime.ToUniversalTimeString());

        }

        

        #endregion

        #region Publics Methods

        /// <summary>
        /// Get a list of <see cref="SD.EventLogEntry"/> by query
        /// </summary>
        /// <returns>
        /// List of <see cref="SD.EventLogEntry"/>
        /// </returns>
        public IEnumerable<SD.EventLogEntry> GetEventLogEntry() {

            IEnumerable<SD.EventLogEntry> entries = null;

            return entries;

        }

        #endregion

    }

}
